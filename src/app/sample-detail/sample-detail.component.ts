import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SampleServiceService } from '../sample-service.service';
import { SampleModel } from '../test4/test4.component';

@Component({
  selector: 'app-sample-detail',
  templateUrl: './sample-detail.component.html',
  styleUrls: ['./sample-detail.component.css']
})
export class SampleDetailComponent implements OnInit {
  sample = new SampleModel();

  constructor(
    private dialogRef: MatDialogRef<SampleDetailComponent>, @Inject(MAT_DIALOG_DATA) public data: any,
    private sampleService: SampleServiceService) { }

  ngOnInit(): void {
    if(this.data.key){
      this.getSample();
    }
  }

  getSample(){
    this.sampleService.getSample(this.data.key).subscribe((response) => {
      this.sample = response['detail'];
    });
  }

  save(){
    this.sampleService.save(this.sample).subscribe(
      response => {
        alert('save success!')
      },
      error => {
        alert('save false!')
      }
    );
  }

  delete(){
    this.sampleService.delete(this.sample).toPromise().then((response) => {
      this.close();
    });
  }

  close(){
    this.dialogRef.close();
  }
}

