import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { Test2RoutingModule } from './test2-routing.module';
import { Test2Component } from './test2.component';
import { Test3Component } from './test3/test3.component';


@NgModule({
  declarations: [
    Test2Component,
    Test3Component
  ],
  imports: [
    CommonModule,
    Test2RoutingModule
  ],
  exports: [
    Test2Component
  ]
})
export class Test2Module { }
