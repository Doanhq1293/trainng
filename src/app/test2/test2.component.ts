import { Component, OnInit } from '@angular/core';
import { Model, SampleServiceService } from '../sample-service.service';

@Component({
  selector: 'app-test2',
  templateUrl: './test2.component.html',
  styleUrls: ['./test2.component.css']
})
export class Test2Component implements OnInit {

  str = '';

  obj = new Model();

  constructor(
    private sampleService: SampleServiceService
  ) { }

  ngOnInit(): void {
    this.str = this.sampleService._serviceProperty;
    this.obj = this.sampleService.serviceObj;
  }

}
