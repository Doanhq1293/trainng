import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Test2Component } from './test2.component';
import { Test3Component } from './test3/test3.component';

const routes: Routes = [
  {
    path: '', component: Test2Component,
    children: [
      {path: 'test3', component: Test3Component},
    ]
  },
  // {path: 'test2', component: Test2Component},
  // {path: 'test3', component: Test3Component},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class Test2RoutingModule { }
