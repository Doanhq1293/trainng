import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { Test1Component } from './test1/test1.component';
import { Test2Component } from './test2/test2.component';

const routes: Routes = [
  {path: '', component: Test1Component},
  {path: 'test1', component: Test1Component},
  // {path: 'test2', component: Test2Component}
  {
    path: 'test2',
    loadChildren: () =>
      import('./test2/test2.module').then((m) => m.Test2Module),
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      preloadingStrategy: PreloadAllModules,
    }),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
