import { Component, ElementRef, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { SampleServiceService } from './sample-service.service';
import { TestService } from './test.service';
import { Test4Component } from './test4/test4.component';

@Component({
  selector: 'app-root',
  // providers: [TestService],
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'trainS2';

  tmpStr = 'doan';
  tmpCode = '123456789012';

  tmpDate = new Date();

  tmpStrTest = ''

  @ViewChild(Test4Component) test4Component!: Test4Component;
  @ViewChildren(Test4Component) test4List!: QueryList<Test4Component>;

  @ViewChild('chartContainer') container!: ElementRef<HTMLDivElement>;

  @ViewChild('tagInp') tagInp!: ElementRef<HTMLDivElement>;

  constructor(
    private sampleService: SampleServiceService,
    private testService: TestService
  ) { }

  ngOnInit(): void {
    console.log('test4Component', this.test4Component);
    // console.log('test4Components', this.test4List);
    console.log('container', this.container);

    this.tmpStrTest = this.testService.str;
  }

  ngAfterViewInit(){
    console.log('test4Component   AAA', this.test4Component);
    // console.log('test4ComponentsAAA', this.test4List);
    console.log('containerAA', this.container);

  }

  testFunc(){
    // this.tagInp.nativeElement.focus();
    // alert(this.tmpStr)
    this.sampleService.setServiceObj('1','testName')
  }

}
