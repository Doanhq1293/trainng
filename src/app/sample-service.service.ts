import { DatePipe } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class SampleServiceService {
  _serviceProperty  = 'str in service'

  _serviceObj = new Model();

  yn = '';

constructor(
  private datepipe: DatePipe,
  private httpClient: HttpClient) { }

  convertDate(date: string | number | Date, format: string) {
    if (!date) {
      return "";
    }

    if (format == null) {
      return this.datepipe.transform(date, "yyyyMMdd");
    }else {
      return this.datepipe.transform(date, format);
    }
  }

  setServiceObj(key: string, name: string){
    this._serviceObj.key = key;
    this._serviceObj.name = name;
  }

  get serviceObj(){
    return this._serviceObj;
  }

  // httpclient
  getSamples(request: any) {
    // ?key=1&sampleName=d
    return this.httpClient.get<any>(environment.apiURL + '/sample/getSamples');
  }

  getSample(key: any) {
    return this.httpClient.get<any>(environment.apiURL + '/sample/getSample?key='+key);
  }

  save(request: any) {
    return this.httpClient.post<any>(environment.apiURL + '/sample/saveSample', request);
  }

  delete(request: any) {
    return this.httpClient.post<any>(environment.apiURL + '/sample/deleteSample', request);
  }

  // bdb
  getCRUD(request: any) {
    // ?key=1&sampleName=d
    return this.httpClient.get<any>(environment.apiURL + '/sample/getCRUD?crudType='+request);
  }

  getCRUDInservice(req){
    debugger
    let yn = '';
    this.getCRUD(req).subscribe((response) => {
      yn = response['crudType'];
    });
    return yn;
  }

  getCRUDCallBack(req, callBackFunc: (obj: Object) => any){
    let yn = '';
    this.getCRUD(req).subscribe((response) => {
      yn = response['crudType'];
      callBackFunc(yn);
    });
  }
  // toPromise().then
  async getCRUDInCom(req){
    debugger
    let yn = '';
    await this.getCRUD(req).toPromise().then((response) => {
      yn = response['crudType'];
    });

    return yn
  }

  getCRUDInComPro(req): Promise<any>{
    return new Promise<void>((resolve, reject) => {
      this.getCRUD(req).subscribe((response) => {
        this.yn = response['crudType'];
        resolve();
      });
    });
  }

}

export class Model{
  key!: string;
  name!: string;
}
