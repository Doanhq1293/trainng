import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'cardNoPipe'})
export class CardNoPipe implements PipeTransform  {

  constructor() { }

  transform(str:string, type: string = 'cardNo'): string {
    let result = ''
    if(str != ''){
      let arr = str.split('');
      let cnt = 0;
      if(type == 'cardNo'){
        arr.forEach(element => {
          cnt++;
          if(cnt%4 == 0 && cnt<str.length){
            result += element + "-";
          }else{
            result += element;
          }
        });
      }else if(type == 'BIZNO'){
        result = str.substring(0, 3) + "-" + str.substring(3, 5) + "-" + str.substring(5, str.length + 1); 
      }
    }
    return result;
  }
}
