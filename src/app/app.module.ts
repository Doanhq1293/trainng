import { DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { ButtonModule } from '@syncfusion/ej2-angular-buttons';
import { TextBoxModule } from '@syncfusion/ej2-angular-inputs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CardNoPipe } from './cardNo.pipe';
import { HelloComponent } from './hello.component';
import { Test1Component } from './test1/test1.component';
import { Test2Module } from './test2/test2.module';
import { Test4Component } from './test4/test4.component';
import { HttpClientModule } from '@angular/common/http';
import { MatDialogModule } from "@angular/material/dialog";
import { SampleDetailComponent } from './sample-detail/sample-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    HelloComponent,
    Test1Component,
    Test4Component,

    CardNoPipe,
      SampleDetailComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    MatDialogModule,
    BrowserAnimationsModule,

    FormsModule,

    ButtonModule,
    TextBoxModule,

    Test2Module
  ],
  providers: [
    DatePipe,
    // TestService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
