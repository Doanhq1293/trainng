import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { ButtonComponent } from '@syncfusion/ej2-angular-buttons';
import { SampleDetailComponent } from '../sample-detail/sample-detail.component';
import { Model, SampleServiceService } from '../sample-service.service';

@Component({
  selector: 'app-test4',
  templateUrl: './test4.component.html',
  styleUrls: ['./test4.component.css']
})
export class Test4Component implements OnInit {

  aaa = '';

  str1 = 'Day la data';
  num1 = 2;
  num2 = 4;
  obj1 = {
    name: 'Nguyen Van A',
    age: 18

  };

  flag1 = true;
  flag2 = false;

  @Input() tmpStr = '';
  @Output() tmpStrChange = new EventEmitter();


  arrData = [
    {
      "id": 1,
      "title": "Training",
      "startDate": "2021-07-12",
      "endDate": "2021-08-11",
      "status": "In-progress",
      "issues": [
        {
          "id": 10,
          "title": "Task 1 for Nat",
          "startDate": "2021-07-22",
          "endDate": "2021-08-01",
          "assignee": {
            "id": 101,
            "username": "nat",
            "email": "nat@a2m.com.vn",
            "age": 30
          }
        },
        {
          "id": 11,
          "title": "Task 2 for Nat",
          "startDate": "2021-07-27",
          "endDate": "2021-08-06",
          "assignee": {
            "id": 101,
            "username": "nat",
            "email": "nat@a2m.com.vn",
            "age": 30
          }
        },
        {
          "id": 12,
          "title": "Task for Kay",
          "startDate": "2021-07-22",
          "endDate": "2021-08-01",
          "assignee": {
            "id": 100,
            "username": "kay",
            "email": "kay@a2m.com.vn",
            "age": 20
          }
        }
      ]
    },
    {
      "id": 2,
      "title": "TTTTTT",
      "startDate": "2021-07-12",
      "endDate": "2021-08-11",
      "status": "Success",
    }
  ]

  // service
  serviceProperty = '';
  sampleDatas: any[] = [];

  ddd = {};

  rawDate: any;
  tmpDate = '';
  // bdb
  crudtype = 'Y';
  myPromise: any;

  // @ViewChild("tmpInp") tmpInp!: ElementRef;

  constructor(
    private dialog: MatDialog,
    private sampleService: SampleServiceService
  ) { }

  async ngOnInit(): Promise<void> {
    debugger
    // this.tmpInp.nativeElement.focus();
    this.serviceProperty = this.sampleService._serviceProperty;
    // this.sampleService._serviceProperty = 'thay doi o appComponent';
    this.ddd['aaaa'] = 'sss';

    this.rawDate = new Date();
    this.tmpDate = this.sampleService.convertDate(new Date(), 'yyyy-MM-dd');

    // this.getSamples();

    // normal
    // this.crudtype = this.sampleService.getCRUDInservice('N');
    // if(this.crudtype != 'N'){
    //   this.getSamples();
    // }

    // callback
    // this.sampleService.getCRUDCallBack('N', (data: Object) => {
    //   this.crudtype = data.toString();
    //   if(this.crudtype != 'N'){
    //     this.getSamples();
    //   }
    // });

    // promise
    // this.sampleService.getCRUDInComPro('Y')
    //   .then(resolve => {
    //     // return resolve;
    //     this.crudtype = this.sampleService.yn;
    //     if(this.crudtype != 'N'){
    //       this.getSamples();
    //     }
    //   }
    // );

    // await
    // this.crudtype = await this.sampleService.getCRUDInCom('N');
    // await this.tmpFunc1(1,2);
    // await this.tmpFunc1(2,3);
    // await this.tmpFunc1(1,2);
    // await this.tmpFunc1(1,2);

    let results = await Promise.all([ this.tmpFunc1(1,2), this.tmpFunc1(1,2), this.tmpFunc1(1,2) ]);

    if(this.crudtype != 'N'){
      this.getSamples();
    }

  }

  getSamples(){
    this.sampleService.getSamples('').subscribe((response) => {
      this.sampleDatas = response['list'];
    });
  }

  openDetail(key){
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.width = '500px';
    dialogConfig.data = {
      key: key,
    };
    const dialogRef = this.dialog.open(SampleDetailComponent, dialogConfig);
    dialogRef.afterClosed().subscribe(
      data =>{
        this.getSamples();
      }
    );
  }

  tmpFunc1(num1: number, num2: number){
    // return num1 + num2;
    let res = 0;
    for (let index = 1; index < 10; index++) {
      res+=index;
    }

    return res
  }

  showInfo(event: any){
    alert('event binding');
    console.log('e', event);
    console.log('data', this.obj1);

  }

  inpKeyup(event: any){
    console.log('inp', event)
  }

  test4Func(){
    alert('vao day')
  }

  changeTmp(event: any){
    console.log('event', event)
    this.tmpStr = event;
    this.tmpStrChange.emit(this.tmpStr)
  }

}

export class SampleModel{
  key!: number;
  sampleName!: string;
  sampleContext!: string;
  lang = '';
  createUser = 1;
}
